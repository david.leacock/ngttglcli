use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fs;
use std::hash::Hash;
use std::io::Write;
use std::time::{Duration, SystemTime};

use gitlab::api::common::NameOrId;
use gitlab::api::endpoint_prelude::Method;
use gitlab::api::*;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};

use derive_builder::Builder;

pub const GROUP_NAME: &str = "cpc-tracktrace";

pub const PROJECTS_CACHE_FILE_NAME: &str = ".ngttglcli_projects";

pub const PROJECTS_CACHE_MAX_AGE_SECONDS: u64 = 60 * 60 * 24;

pub const SCAN_CACHE_FILE_NAME: &str = ".ngttglcli_scan";

pub const SCAN_CACHE_MAX_AGE_SECONDS: u64 = 60 * 60 * 24;

#[derive(Debug)]
pub enum FailureMode {
    VarError(std::env::VarError),
    GitlabError(gitlab::GitlabError),
}

impl std::fmt::Display for FailureMode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FailureMode::VarError(inner) => {
                write!(f, "Error reading $GITLAB_PAT: {}", inner)
            }
            FailureMode::GitlabError(inner) => {
                write!(
                    f,
                    "Error connecting to GitLab: {}. (Is your $GITLAB_PAT correct?)",
                    inner
                )
            }
        }
    }
}

// See: https://docs.rs/gitlab/latest/gitlab/types/struct.Project.html
#[derive(Serialize, Deserialize, Clone)]
pub struct Project {
    pub name: String,
    pub id: gitlab::ProjectId,
    pub path_with_namespace: String,
}

// See: https://docs.rs/gitlab/latest/gitlab/types/struct.MergeRequest.html
#[derive(Serialize, Deserialize, Clone)]
pub struct MergeRequest {
    pub iid: gitlab::MergeRequestInternalId,
    pub title: String,
}

// overridden because below, in Branch, we only need the Commit title
#[derive(Serialize, Deserialize, Clone)]
pub struct Commit {
    pub title: String,
}

// See: https://docs.rs/gitlab/latest/gitlab/types/struct.RepoBranch.html
#[derive(Serialize, Deserialize, Clone)]
pub struct Branch {
    pub name: String,
    pub commit: Option<Commit>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ScanResult {
    pub project: Project,
    pub mrs: Vec<MergeRequest>,
    pub branches: Vec<Branch>,
}

/// Creates a connection to gitlab.com using the provided GitLab PAT
pub fn connect() -> Result<gitlab::Gitlab, FailureMode> {
    let token = match std::env::var("GITLAB_PAT") {
        Err(error) => return Err(FailureMode::VarError(error)),
        Ok(token) => token,
    };

    let client = match gitlab::Gitlab::new("gitlab.com", token) {
        Err(error) => return Err(FailureMode::GitlabError(error)),
        Ok(client) => client,
    };

    return Ok(client);
}

/// Queries GitLab for all projects under the GROUP_NAME and its subgroups.
///
/// If the `search` term is not [None], only the subset of projects which match that search term will be fetched.
fn fetch_projects_without_cache(
    client: &gitlab::Gitlab,
    search: Option<&str>,
) -> HashMap<String, Project> {
    let mut builder = groups::projects::GroupProjects::builder();
    let builder = builder.group(GROUP_NAME).include_subgroups(true);

    if let Some(term) = search {
        builder.search(term);
    }

    let pageable_endpoint = builder.build().unwrap();

    let projects: Vec<Project> = paged(pageable_endpoint, Pagination::All)
        .query(client)
        .unwrap();

    projects.into_iter().map(|p| (p.name.clone(), p)).collect()
}

/// Attempts to read the JSON-formatted data from the specified file.
///
/// Will return [Err] if the file's data or metadata cannot be read, or if the cache is older than the `cache_max_age`.
fn read_from_cache<K, V>(
    cache_file_name: &str,
    cache_max_age: Duration,
) -> Result<HashMap<K, V>, String>
where
    K: Serialize + DeserializeOwned + Eq + Hash,
    V: Serialize + DeserializeOwned,
{
    // only if the cache file exists, can be read, and its modification time is <= max, do we use it
    let cache_age = fs::metadata(cache_file_name)
        .map_err(|error| error.to_string())
        .and_then(|metadata| metadata.modified().map_err(|error| error.to_string()))
        .and_then(|file_time| {
            SystemTime::now()
                .duration_since(file_time)
                .map_err(|error| error.to_string())
        });

    // only if the cache can be read and correctly converted to a Vec<T>, do we use it
    let cache_contents = fs::read_to_string(cache_file_name)
        .map_err(|error| error.to_string())
        .and_then(|contents| serde_json::from_str(&contents).map_err(|error| error.to_string()));

    match (cache_age, cache_contents) {
        (Ok(age), contents) if age.le(&cache_max_age) => contents,
        (Ok(age), _) => Err(format!(
            "expired cache (age: {:?}s, max age: {:?}s)",
            age.as_secs(),
            cache_max_age.as_secs()
        )),
        (Err(msg), _) => Err(msg),
    }
}

/// Writes the `data` to the specified file in JSON format
fn overwrite_cache<K, V>(cache_file_name: &str, data: &HashMap<K, V>) -> Result<(), String>
where
    K: Serialize + DeserializeOwned + Eq + Hash,
    V: Serialize + DeserializeOwned,
{
    serde_json::to_string_pretty(data)
        .map_err(|_| String::from("Failed to serialize to JSON"))
        .and_then(|serialized| {
            fs::write(cache_file_name, serialized).map_err(|error| error.to_string())
        })
}

/// Updates some (but not all) of the cache.
///
/// Because some of the cache will not be updated, the file modification time is purposefully not updated.
///
/// This means that the age of the cache will not be reset, and it will expire at the expected time, as though it had not been updated.
fn update_cache<K, V>(
    cache_file_name: &str,
    cache_max_age: Duration,
    patch: &HashMap<K, V>,
) -> HashMap<K, V>
where
    K: Serialize + DeserializeOwned + Eq + Hash + Clone,
    V: Serialize + DeserializeOwned + Clone,
{
    // save the current modification time of the file
    let mtime = fs::metadata(cache_file_name).and_then(|metadata| metadata.modified());

    // get the current cache
    let mut cache: HashMap<K, V> =
        read_from_cache(cache_file_name, cache_max_age).unwrap_or_else(|_| HashMap::new());

    // update the cache with the patch
    for (key, value) in patch {
        cache.insert(key.clone(), value.clone());
    }

    // rewrite the file with the patched cache
    overwrite_cache(cache_file_name, &cache).expect("Failed to update cache");

    // set the modification time of the file to the original mtime
    // sometimes fails if we only just created the file above (race condition?)
    let _ = mtime
        .and_then(|time| filetime::set_file_mtime(cache_file_name, filetime::FileTime::from(time)));

    cache
}

/// Uses the cache to return a list of [Project]s.
///
/// If `force_cache_update` is `true`, all projects will be fetched from GitLab, and the cache will be overwritten with that information.
///
/// Otherwise, if the search term is not [None], only the subset of projects which match that search term will be fetched from GitLab, and the cache will be updated with that information. The refresh time of the cache will be unaffected.
///
/// Otherwise, if the cache cannot be read, all projects will be fetched from GitLab.
///
/// Otherwise, the cache will be returned.
pub fn fetch_projects(
    client: &gitlab::Gitlab,
    force_cache_update: bool,
    search: Option<&str>,
) -> HashMap<String, Project> {
    if force_cache_update {
        println!("Performing a forced cache update of the list of projects...");
        let projects = fetch_projects_without_cache(client, None);
        overwrite_cache(PROJECTS_CACHE_FILE_NAME, &projects).expect("Failed to write to cache");
        projects
    } else if let Some(_) = search {
        let projects = fetch_projects_without_cache(client, search);
        update_cache(
            PROJECTS_CACHE_FILE_NAME,
            Duration::from_secs(PROJECTS_CACHE_MAX_AGE_SECONDS),
            &projects,
        );
        projects
    } else {
        read_from_cache(
            PROJECTS_CACHE_FILE_NAME,
            Duration::from_secs(PROJECTS_CACHE_MAX_AGE_SECONDS),
        )
        .unwrap_or_else(|error| {
            eprintln!("Failed to read projects from cache due to: {}", error);
            let projects = fetch_projects_without_cache(client, None);
            overwrite_cache(PROJECTS_CACHE_FILE_NAME, &projects).expect("Failed to write to cache");
            projects
        })
    }
}

/// Queries GitLab for any open Scala Steward MRs for the specified project
fn fetch_open_scala_steward_mrs(client: &gitlab::Gitlab, project: &Project) -> Vec<MergeRequest> {
    let pageable_endpoint = projects::merge_requests::MergeRequests::builder()
        .project(project.id.value())
        .search("NGTT-13308") // this is how we distinguish Scala Steward MRs
        .state(projects::merge_requests::MergeRequestState::Opened)
        .build()
        .unwrap();

    paged(pageable_endpoint, Pagination::All)
        .query(client)
        .unwrap()
}

/// Queries GitLab for any Scala Steward branches for the specified project
fn fetch_scala_steward_branches(client: &gitlab::Gitlab, project: &Project) -> Vec<Branch> {
    let pageable_endpoint = projects::repository::branches::Branches::builder()
        .project(project.id.value())
        .search("update/") // this is how we distinguish Scala Steward branches
        .build()
        .unwrap();

    paged(pageable_endpoint, Pagination::All)
        .query(client)
        .unwrap()
}

/// Queries GitLab for any Scala Steward branches and open Scala Steward MRs for the specified project and returns them in a ScanResult
pub fn scan_without_cache(client: &gitlab::Gitlab, project: &Project) -> ScanResult {
    let mrs = fetch_open_scala_steward_mrs(client, project);
    let branches = fetch_scala_steward_branches(client, project);

    ScanResult {
        project: project.clone(),
        mrs,
        branches,
    }
}

/// [Scan](scan_without_cache)s all projects.
///
/// If `force_project_cache_update` is set to `true`, the list of projects will be re-fetched before scanning.
fn scan_all_without_cache(
    client: &gitlab::Gitlab,
    force_project_cache_update: bool,
    search: Option<&str>,
) -> HashMap<String, ScanResult> {
    let projects = fetch_projects(client, force_project_cache_update, search);
    let n_projects = projects.len();

    print!("Progress: |{}|", " ".repeat(n_projects));
    std::io::stdout().flush().unwrap(); // if the buffer doesn't flush, then... ???

    let results = projects
        .values()
        .enumerate()
        .map(|(index, project)| {
            print!(
                "\rProgress: |{}{}|",
                "=".repeat(index + 1),
                " ".repeat(n_projects - (index + 1))
            );
            std::io::stdout().flush().unwrap(); // if the buffer doesn't flush, then... ???
            project
        })
        .map(|project| scan_without_cache(client, project))
        .collect::<Vec<ScanResult>>();

    println!();
    results
        .into_iter()
        .map(|r| (r.project.id.value().to_string(), r))
        .collect()
}

/// Uses the caches to scan a list of [Project]s.
///
/// If `force_scan_cache_update` is `true`, all branches and MRs will be fetched from GitLab, and the cache will be overwritten with that information.
///
/// Otherwise, if the `search` term is not [None], only the subset of projects which match that search term will be fetched from GitLab, and the cache will be updated with that information. The refresh time of the cache will be unaffected.
///
/// Otherwise, if the cache cannot be read, all branches and MRs will be fetched from GitLab.
///
/// Otherwise, the cache will be returned.
pub fn scan_all(
    client: &gitlab::Gitlab,
    force_project_cache_update: bool,
    force_scan_cache_update: bool,
    search: Option<&str>,
) -> HashMap<String, ScanResult> {
    if force_scan_cache_update {
        println!("Performing a forced cache update of the list of scan results...");
        let scan_results = scan_all_without_cache(client, force_project_cache_update, None);
        overwrite_cache(SCAN_CACHE_FILE_NAME, &scan_results).expect("Failed to write to cache");
        scan_results
    } else if let Some(_) = search {
        let scan_results = scan_all_without_cache(client, force_project_cache_update, search);
        update_cache(
            SCAN_CACHE_FILE_NAME,
            Duration::from_secs(SCAN_CACHE_MAX_AGE_SECONDS),
            &scan_results,
        );
        scan_results
    } else {
        read_from_cache(
            SCAN_CACHE_FILE_NAME,
            Duration::from_secs(SCAN_CACHE_MAX_AGE_SECONDS),
        )
        .unwrap_or_else(|error| {
            eprintln!("Failed to read scan results from cache due to: {}", error);
            let scan_results = scan_all_without_cache(client, force_project_cache_update, None);
            overwrite_cache(SCAN_CACHE_FILE_NAME, &scan_results).expect("Failed to write to cache");
            scan_results
        })
    }
}

pub fn clean_project(client: &gitlab::Gitlab, result: &ScanResult) {
    println!();

    for mr in result.mrs.iter() {
        println!("    Deleting MR #{}: {}", mr.iid, mr.title);

        let endpoint = projects::merge_requests::EditMergeRequest::builder()
            .project(result.project.id.value())
            .merge_request(mr.iid.value())
            .state_event(projects::merge_requests::MergeRequestStateEvent::Close)
            .build()
            .unwrap();

        ignore(endpoint).query(client).unwrap()
    }

    for branch in result.branches.iter() {
        println!(
            "    Deleting branch {} -- \"{}\"",
            branch.name,
            branch
                .commit
                .as_ref()
                .map(|c| c.title.as_str())
                .unwrap_or_else(|| "")
        );

        let endpoint: DeleteBranch = DeleteBranch::builder()
            .project(result.project.id.value())
            .branch(branch.name.clone())
            .build()
            .unwrap();

        ignore(endpoint).query(client).unwrap()
    }
}

// ---

/// Query for a specific branch in a project.
#[derive(Debug, Builder, Clone)]
pub struct DeleteBranch<'a> {
    /// The project to get a branch from.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The branch to get.
    #[builder(setter(into))]
    branch: Cow<'a, str>,
}

impl<'a> DeleteBranch<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> DeleteBranchBuilder<'a> {
        DeleteBranchBuilder::default()
    }
}

impl<'a> Endpoint for DeleteBranch<'a> {
    fn method(&self) -> Method {
        Method::DELETE
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!(
            "projects/{}/repository/branches/{}",
            self.project,
            common::path_escaped(&self.branch),
        )
        .into()
    }
}
