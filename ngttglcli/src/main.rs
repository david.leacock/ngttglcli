use std::io::{stdin, stdout, Write};

use rustyline::error::ReadlineError;
use rustyline::DefaultEditor;

use clap::Parser;
use ngttglcli::*;

fn main() -> Result<(), FailureMode> {
    /// Run ngttglcli with no arguments to enter the REPL
    #[derive(Parser, Debug)]
    #[command(version)]
    struct Args {}

    // only --version and --help CL args so far
    let _args = Args::parse();

    print!("Connecting to {}... ", GROUP_NAME);
    stdout().flush().unwrap();

    let client = match connect() {
        Err(error) => {
            eprintln!("\n{}", error);
            std::process::exit(1)
        }
        Ok(client) => client,
    };

    println!("connected!");

    // `()` can be used when no completer is required
    let mut rl = DefaultEditor::new().expect("ReadlineError trying to create REPL");

    if rl.load_history(".ngttglcli_history").is_err() {
        println!("Unable to load REPL history.");
    }

    fn report_result(result: &ScanResult, verbose: bool, hide_empty_results: bool) {
        let n_mrs = result.mrs.len();
        let n_branches = result.branches.len();

        if !(hide_empty_results && n_mrs < 1 && n_branches < 1) {
            if verbose {
                println!()
            }

            let report_mrs = if hide_empty_results && n_mrs < 1 {
                String::from("")
            } else {
                format!("{} open Scala Steward MR(s)", n_mrs)
            };

            let report_branches = if hide_empty_results && n_branches < 1 {
                String::from("")
            } else {
                format!("{} Scala Steward branch(es)", n_branches)
            };

            let and = if report_mrs.is_empty() || report_branches.is_empty() {
                ""
            } else {
                " and "
            };

            println!(
                "  {}: {} has {}{}{}",
                result.project.id, result.project.name, report_mrs, and, report_branches
            );

            if verbose && n_mrs > 0 {
                println!("  MRs:");
                result
                    .mrs
                    .iter()
                    .for_each(|mr| println!("    MR #{}: {}", mr.iid, mr.title))
            }

            if verbose && n_branches > 0 {
                println!("  Branches:");
                result.branches.iter().for_each(|branch| {
                    println!(
                        "    {} -- \"{}\"",
                        branch.name,
                        branch
                            .commit
                            .as_ref()
                            .map(|c| c.title.as_str())
                            .unwrap_or_else(|| "")
                    )
                })
            }
        }
    }

    fn print_help() {
        println!("Available commands:\n  {}\n  {}\n  {}\n  {}\n  {}\n  {}\n  {}\n  {}\n  {}\n  {}",
                   "help                  -- prints this help menu",
                   "fetch all             -- query GitLab for the list of projects and rewrite the cache",
                   "fetch <search term>   -- query GitLab for projects matching the search term and update them in the cache",
                   "projects              -- prints a list of known projects with their IDs",
                   "scan all              -- query GitLab for Scala Steward MRs and branches for all projects and rewrite the cache",
                   "scan <search term>    -- query GitLab for Scala Steward MRs and branches for projects matching the search term and update them in the cache",
                   "report all            -- prints a scan report every project with a nonzero number of Scala Steward MRs or branches",
                   "report <project id>   -- prints a scan report for the project with the specified ID",
                   "clean <project id>    -- closes Scala Steward MRs and deletes Scala Steward branches for the project with the specified ID",
                   "quit                  -- close the REPL (also: 'exit')"
        )
    }

    loop {
        let readline = rl.readline("\n>> ");
        match readline {
            Ok(line) => {
                match rl.add_history_entry(line.as_str()) {
                    Ok(true) => (),  // added successfully
                    Ok(false) => (), // not added because duplicate line
                    Err(_) => eprintln!("Unable to add line to REPL history"),
                }

                match line.as_str() {
                    "help" => print_help(),

                    fetch if fetch.starts_with("fetch ") => {
                        let term = fetch.strip_prefix("fetch").unwrap().trim();

                        if term == "all" {
                            fetch_projects(&client, true, None); // takes ~12 seconds
                        } else {
                            let result = fetch_projects(&client, false, Some(term));

                            println!("Search returned these projects...");
                            result.values().for_each(|project| {
                                println!(
                                    "  {}: {} @ {}",
                                    project.id, project.name, project.path_with_namespace
                                );
                            });
                            println!("...updated them in the cache.")
                        }
                    }

                    scan if scan.starts_with("scan ") => {
                        let term = scan.strip_prefix("scan").unwrap().trim();

                        if term == "all" {
                            let mut scan_results: Vec<ScanResult> =
                                scan_all(&client, true, true, None).into_values().collect();
                            scan_results
                                .sort_by_key(|result| (result.mrs.len(), result.branches.len()));
                            scan_results.reverse();
                            scan_results
                                .iter()
                                .for_each(|result| report_result(result, false, false))
                        } else {
                            let mut scan_results: Vec<ScanResult> =
                                scan_all(&client, false, false, Some(term))
                                    .into_values()
                                    .collect();
                            scan_results
                                .sort_by_key(|result| (result.mrs.len(), result.branches.len()));
                            scan_results.reverse();

                            println!("Search returned these projects...");
                            scan_results
                                .iter()
                                .for_each(|result| report_result(result, false, false));
                            println!("...updated them in the cache.")
                        }
                    }

                    "projects" => {
                        let result = fetch_projects(&client, false, None);

                        result.values().for_each(|project| {
                            println!(
                                "  {}: {} @ {}",
                                project.id, project.name, project.path_with_namespace
                            );
                        });
                    }

                    report if report.starts_with("report ") => {
                        let id = report.strip_prefix("report").unwrap().trim();

                        let scan_results = scan_all(&client, false, false, None);

                        if id == "all" {
                            let mut scan_results: Vec<ScanResult> =
                                scan_results.into_values().collect();
                            scan_results
                                .sort_by_key(|result| (result.mrs.len(), result.branches.len()));
                            scan_results.reverse();
                            scan_results
                                .iter()
                                .for_each(|result| report_result(result, true, true));

                            let (total_mrs, total_branches) = scan_results.into_iter().fold(
                                (0, 0),
                                |(total_mrs, total_branches), result| {
                                    (
                                        total_mrs + result.mrs.len(),
                                        total_branches + result.branches.len(),
                                    )
                                },
                            );

                            println!("\nTotal Scala Steward MRs open: {}", total_mrs);
                            println!("Total Scala Steward branches: {}", total_branches);
                        } else if let Some(result) = scan_results.get(id) {
                            report_result(result, true, false)
                        } else {
                            eprintln!("Unknown project ID '{}'. Please run 'projects' to see all projects with IDs.", id)
                        }
                    }

                    clean if clean.starts_with("clean ") => {
                        let id = clean.strip_prefix("clean").unwrap().trim();

                        let projects = fetch_projects(&client, false, None);

                        if let Some(project) = projects
                            .into_values()
                            .filter(|p| p.id.to_string() == id)
                            .nth(0)
                        {
                            let result = scan_without_cache(&client, &project);
                            report_result(&result, true, false);

                            if result.mrs.len() < 1 && result.branches.len() < 1 {
                                println!("\n  Nothing to clean")
                            } else {
                                let confirmation = "I! DECLARE! BANKRUPTCY!";

                                println!("\n  !!! WARNING !!!");
                                println!("    Cleaning this project will close all of the MRs and delete all of the branches listed above.");
                                println!("    Are you sure you wish to continue? If so, you must enter the following line exactly as it appears below:\n");
                                println!("    >> {}", confirmation);

                                let mut input = String::new();
                                print!("    >> ");
                                let _ = stdout().flush();
                                let _ = stdin().read_line(&mut input);

                                if input.trim() == confirmation {
                                    println!("\n  CONFIRMED. Cleaning now...");
                                    clean_project(&client, &result);
                                } else {
                                    println!("\n  Cleaning cancelled.")
                                }
                            }
                        } else {
                            eprintln!("Unknown project ID '{}'. Please run 'projects' to see all projects with IDs.", id)
                        }
                    }

                    "quit" | "exit" => break,

                    unknown => {
                        println!("Unknown command: '{}'", unknown);
                        print_help()
                    }
                }
            }

            Err(ReadlineError::Interrupted) => {
                // println!("CTRL-C");
                break;
            }

            Err(ReadlineError::Eof) => {
                // println!("CTRL-D");
                break;
            }

            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }

    rl.save_history(".ngttglcli_history")
        .expect("Unable to save REPL history.");

    Ok(())
}
