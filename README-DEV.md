# `ngttglcli` Development Notes

## One-Time Setup

If you do not want to package the code for distribution (if you just want to develop locally), then the only subsection you need to follow from this section is `Cargo`.

If you want to create executables to be run on other machines, you will need to follow all subsections here.

### Cargo

This repo can be compiled from source with Cargo, the Rust build tool.

Cargo is also Rust's package ("crate") manager.

You can [install Cargo (and `rustup`, the Rust installer) with](https://doc.rust-lang.org/cargo/getting-started/installation.html)

```shell
curl https://sh.rustup.rs -sSf | sh
```

Verify that Cargo was installed correctly with

```shell
cargo --version
```

The output should look something like

```
cargo 1.70.0 (ec8a8a0ca 2023-04-25)
```

Try building this repo by running the following command in the same directory as this `README`

```shell
cargo build
```

### Cross

If you want to cross-compile this repo to create executables which can be run on Intel and Apple Silicon machines, you'll need to install `cross` next.

`cross` is a Rust crate for cross-compilation. [Install it with](https://github.com/cross-rs/cross#installation)

```shell
cargo install cross --git https://github.com/cross-rs/cross
```

This will install `cross` in your `cargo` installation's `bin` directory (usually `$HOME/.cargo/bin`).

### macOS SDK

Cross requires Docker container images built with macOS SDKs to do its cross-compilation.

You can [build one of these from source](https://github.com/tpoechtrager/osxcross#packaging-the-sdk) as well, but there are also prepackaged macOS SDKs available online, [like at this repo](https://github.com/joseluisq/macosx-sdks).

Grab the URL of the `tar.xz` file for one of the SDKs listed above and save it in an environment variable, like

```shell
export MACOS_SDK_URL="https://github.com/joseluisq/macosx-sdks/releases/download/12.3/MacOSX12.3.sdk.tar.xz"
```

We will use this in the next step.

> Note: As of this writing, SDK `12.3` from the above repo is known to work, but
>
> - `13.0` fails with `mv: cannot stat '*OSX*13.0*sdk*': No such file or directory`
> - `13.1` fails with an `Unsupported SDK` error

### Building the Docker Container Images

Next, to create the required Docker container images, you'll also need to [clone the `cross` repo](https://github.com/cross-rs/cross-toolchains#cross-toolchains)

```shell
cd $HOME
git clone https://github.com/cross-rs/cross
cd cross
git submodule update --init --remote
```

Still in the `cross/` directory, build the required images with

```shell
# create M1 container image (can take ~40m to build)
cargo build-docker-image aarch64-apple-darwin-cross --tag local --build-arg MACOS_SDK_URL=$MACOS_SDK_URL
```

```shell
# create Intel container image (fast if layers built above have been cached)
cargo build-docker-image x86_64-apple-darwin-cross --tag local --build-arg MACOS_SDK_URL=$MACOS_SDK_URL
```

Note that these container images are required by the `Cross.toml` file in this project

```toml
[target.x86_64-apple-darwin]
image = "ghcr.io/cross-rs/x86_64-apple-darwin-cross:local"

[target.aarch64-apple-darwin]
image = "ghcr.io/cross-rs/aarch64-apple-darwin-cross:local"
```

Verify that the container images have been correctly created

```shell
docker image list | grep ghcr.io
```

## Building and Running

This project can be built with

```shell
cargo build
```

or built and run with

```shell
cargo run
```

## Publishing

Throughout this section

- **"M1"** is used to refer to the "Apple Silicon" chip architecture, aka. "AArch64" aka. "ARM64", used in newer Apple machines (Nov 2020 onward)
- **"Intel"** is used to refer to the "x86_64" chip architecture, aka. "AMD64", used in older Apple machines

**Make sure you've followed all the "One-Time Setup" steps above before proceeding.**

### Cross-Compiling

We can compile for both the Apple Silicon and Intel architectures using `cross`.

In the root directory of this project (`ngttglcli`), run

```shell
# compile for Intel machines (can take 5m+)
cross build --target x86_64-apple-darwin --release
```

```shell
# compile for M1 machines (can take about 10m)
cross build --target aarch64-apple-darwin --release
```

**NOTE:** If you get an error like

```
$ cross build --target whatever-apple-darwin
Error: 
   0: toolchain is not fully qualified

Location:
   src/rustc.rs:268

Note: cross expects the toolchain to be a rustup installed toolchain
Suggestion: if you're using a custom toolchain try setting `CROSS_CUSTOM_TOOLCHAIN=1` or install rust via rustup

Backtrace omitted. Run with RUST_BACKTRACE=1 environment variable to display it.
Run with RUST_BACKTRACE=full to include source snippets.
```

...this might be because you installed Rust via `homebrew` rather than via `rustup`. Uninstalling with

```shell
brew uninstall rust
```

should fix that problem.

Once finished, you should have two executables in `target/*/release`

```shell
ls -1 target/*/release/ngttglcli
```

Check that they have the correct architecture with `file -b`

```shell
# should print: Mach-O 64-bit executable x86_64
file -b target/x86_64-apple-darwin/release/ngttglcli
```

```shell
# should print: Mach-O 64-bit executable arm64
file -b target/aarch64-apple-darwin/release/ngttglcli
```

### Tarballs and Hashes

To package the binary executables [for distribution via `homebrew`](https://federicoterzi.com/blog/how-to-publish-your-rust-project-on-homebrew/), simply wrap them into tarballs with

```shell
cd target/x86_64-apple-darwin/release && tar -czf ngttglcli-x86_64.tar.gz ngttglcli && cd ../../../
```

```shell
cd target/aarch64-apple-darwin/release && tar -czf ngttglcli-aarch64.tar.gz ngttglcli && cd ../../../
```

You should see two tarballs here, and they should both be a few MB in size

```shell
ls -lh target/*/release/ngttglcli*.tar.gz
```

Calculate the SHA256 hashes of the archives (we'll need these later)

```shell
shasum -a 256 target/*/release/ngttglcli*.tar.gz
```

### Publish to GitLab

If you're releasing a new version, at this point you should update the version number in `Cargo.toml` appropriately. This project uses semantic versioning with no leading `v` (e.g. `0.1.0`).

Then, add the `.tar.gz` archives, the `Cargo.toml` file, and whatever other relevant changes you have made to a commit and push to `master`.

Create a new release on GitLab at [gitlab.com/improving-andrew/ngttglcli/-/releases](https://gitlab.com/improving-andrew/ngttglcli/-/releases), making sure to add the **permalink** URL of each binary to the "Release assets" section at the bottom. See an [example release here](https://gitlab.com/improving-andrew/ngttglcli/-/releases/0.1.0). Set the "Type" of each release asset to "Package".

Make a note of the **RAW** URLs of the `.tar.gz` archives. In this example (`0.1.0`), these are

```
https://gitlab.com/improving-andrew/ngttglcli/-/raw/e19cc1e5cb9f73cf2383174739221a3a3aec1b8a/target/aarch64-apple-darwin/release/ngttglcli-aarch64.tar.gz
```

and

```
https://gitlab.com/improving-andrew/ngttglcli/-/raw/e19cc1e5cb9f73cf2383174739221a3a3aec1b8a/target/x86_64-apple-darwin/release/ngttglcli-x86_64.tar.gz
```

### Publish to Homebrew

Next, update the `ngttglcli.rb` Homebrew formula at [gitlab.com/cpc-tracktrace/homebrew-ngtt/-/blob/master/Formula/ngttglcli.rb](https://gitlab.com/cpc-tracktrace/homebrew-ngtt/-/blob/master/Formula/ngttglcli.rb) with the new URLs and SHAs.

Don't forget to update the `version` there, as well.

Finally, you can `brew upgrade ngttglcli` to get the updated version locally.

## Testing

Calculate test coverage (and generate an HTML report) with

```shell
cargo xtask coverage ngttglcli
```

## Formatting

Check that code is formatted correctly with

```shell
cargo fmt --all -- --check
```

Reformat code with

```shell
cargo fmt
```